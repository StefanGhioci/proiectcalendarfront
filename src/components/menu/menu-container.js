import React from "react";
import NavTabs from "./menu-view";


const MenuContainer = (props) => {
  return <NavTabs {...props}/>
};

export default MenuContainer;
