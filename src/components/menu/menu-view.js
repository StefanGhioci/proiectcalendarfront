import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import EventIcon from '@material-ui/icons/Event';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SubjectContainer from '../pages/subject/subject-container';
import HomepageContainer from '../pages/homepage/homepage-container';
import useStyles from './menu-styles';
import { AuthContext } from '../../context/auth/authContext';
import { SECTION_CALENDAR_INDEX, SECTION_SUBJECTS_INDEX } from '../../utils/constants';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

const NavTabs = props => {
  const { defaultSelectedSection, ...others } = props;
  const classes = useStyles();
  const theme = useTheme();
  const authContext = useContext(AuthContext);
  const [open, setOpen] = React.useState(false);
  const [selectedSection, setSelectedSection] = React.useState(defaultSelectedSection);

  const handleSectionChange = (event, newValue) => {
    setSelectedSection(newValue);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={clsx(classes.appBar, { [classes.appBarShift]: open })}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            Calendar
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Typography align="center">Hello, {authContext.auth.username}</Typography>
        <Divider />
        <List>
          <ListItem button onClick={event => handleSectionChange(event, SECTION_CALENDAR_INDEX)}>
            <ListItemIcon>
              <EventIcon />
            </ListItemIcon>
            <ListItemText>Calendar</ListItemText>
          </ListItem>
          <ListItem button onClick={event => handleSectionChange(event, SECTION_SUBJECTS_INDEX)}>
            <ListItemIcon>
              <MailIcon />
            </ListItemIcon>
            <ListItemText>Subjects</ListItemText>
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button onClick={authContext.logout}>
            <ListItemIcon>
              <ExitToAppIcon />
            </ListItemIcon>
            <ListItemText>Log out</ListItemText>
          </ListItem>
        </List>
      </Drawer>
      <Box id="ceva" className={clsx(classes.content, { [classes.contentShift]: open })}>
        <div className={classes.drawerHeader} />
        <TabPanel value={selectedSection} index={SECTION_CALENDAR_INDEX}>
          <HomepageContainer {...others} />
        </TabPanel>
        <TabPanel value={selectedSection} index={SECTION_SUBJECTS_INDEX}>
          <SubjectContainer {...others} />
        </TabPanel>
      </Box>
    </div>
  );
};

export default NavTabs;

NavTabs.propTypes = {
  defaultSelectedSection: PropTypes.any.isRequired,
};

TabPanel.propTypes = {
  children: PropTypes.node.isRequired,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
