import React, { useState } from 'react';

import { Card, CardContent, CardActions, Button, Grid, Typography } from '@material-ui/core';
import { Field, Form } from 'react-final-form';
import SendIcon from '@material-ui/icons/Send';
import { TextField } from 'final-form-material-ui';
import { useStyles } from './notif-styles';
import { apiRequestUnauthenticated } from '../../utils/dataprovider';

const View = () => {
  const classes = useStyles();
  const [serverMessage, setServerMessage] = useState('');

  const saveNotif = async values => {
    apiRequestUnauthenticated('/notification/add', 'POST', values)
      .then(
        () => setServerMessage('Notification sent successfully!'),
        () => setServerMessage('Error sending notification!'),
      )
      .catch(() => setServerMessage('An error occured!'));
  };

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <Form
          onSubmit={saveNotif}
          render={({ handleSubmit, submitting }) => (
            <form onSubmit={handleSubmit}>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Field
                      component={TextField}
                      variant="outlined"
                      fullWidth
                      color="secondary"
                      id="message"
                      label="Message"
                      name="message"
                      autoComplete="message"
                      autoFocus
                      placeholder="Enter the notification message here"
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <CardActions>
                <Grid container spacing={3} xs={12}>
                  <Grid item container xs={12}>
                    <Button
                      type="submit"
                      color="secondary"
                      className={classes.sendBtn}
                      startIcon={<SendIcon />}
                      disabled={submitting}
                    >
                      Send Notification
                    </Button>
                  </Grid>
                  <Grid container>
                    <Typography className={classes.serverMessage} color="secondary">
                      {serverMessage}
                    </Typography>
                  </Grid>
                </Grid>
              </CardActions>
            </form>
          )}
        />
      </Card>
    </div>
  );
};

export default View;
