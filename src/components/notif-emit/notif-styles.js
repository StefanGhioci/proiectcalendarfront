import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    width: '75%',
    margin: theme.spacing(4),
    padding: theme.spacing(2),
  },
  sendBtn: {
    marginLeft: theme.spacing(1),
  },
  serverMessage: {
    marginLeft: theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
}));
