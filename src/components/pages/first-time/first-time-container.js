/* eslint-disable radix */
import React, { useState, useEffect, useContext } from 'react';
import {
  Stepper,
  Step,
  StepLabel,
  Typography,
  Grid,
  Button,
  Card,
  Container,
} from '@material-ui/core';
import FinishedView from './subcomponents/finished-view';
import SelectGroupView from './subcomponents/select-group-view';
import { useStyles } from './first-time-styles';
import StudentInfoView from './subcomponents/student-info-view';
import {
  apiRequestGroups,
  apiRequestAllDisciplines,
  apiRequest,
} from '../../../utils/dataprovider';
import { AuthContext } from '../../../context/auth/authContext';

const getSteps = () => {
  return ['Fill in your info', 'Select your group'];
};

const FirstTimeContainer = () => {
  const [activeStep, setActiveStep] = useState(0);
  const [skipped, setSkipped] = useState(new Set());
  const steps = getSteps();

  const [nextDisabled, setNextDisabled] = useState(true);
  const [info, setInfo] = useState({ year: '', programme: '', language: '' });
  const [group, setGroup] = useState('');

  const [groups, setGroups] = useState([]);
  const [allDisciplines, setAllDisciplines] = useState([]);
  const [chosenDisciplines, setChosenDisciplines] = useState([]);
  const authContext = useContext(AuthContext);

  const populateDisciplines = async data => {
    setAllDisciplines(await apiRequestAllDisciplines(data));
  };

  const populateGroups = async data => {
    setGroups(await apiRequestGroups(data));
  };

  useEffect(() => {
    if (activeStep === steps.length)
      apiRequest('/student', 'POST', {
        studentEmail: authContext.auth.username,
        groupNumber: parseInt(group),
        specialization: info.programme,
        studyLine: info.language,
        year: parseInt(info.year),
      }).then(() => {
        authContext.setAuthState({ ...authContext.auth, student: "true" });
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeStep]);

  useEffect(() => {
    if (info.year !== '' && info.programme !== '' && info.language !== '') {
      populateGroups(info);
    }
  }, [info]);

  useEffect(() => {
    if (group !== '') {
      populateDisciplines(group);
    }
  }, [group]);

  const handleChooseDiscipline = id => event => {
    const intId = parseInt(id);
    if (event.target.checked === true)
      if (!chosenDisciplines.includes(intId)) setChosenDisciplines([...chosenDisciplines, intId]);
      else setChosenDisciplines(chosenDisciplines.filter(el => el !== intId));
  };

  const addOtherDiscipline = discipline => {
    if (allDisciplines.filter(d => d.id === discipline.id).length === 0)
      setAllDisciplines([...allDisciplines, discipline]);
  };

  const isDisciplineChosen = id => {
    return chosenDisciplines.includes(id);
  };

  useEffect(() => {
    switch (activeStep) {
      case 0: // student info
        setNextDisabled(info.year === '' || info.programme === '' || info.language === '');
        break;
      case 1: // group
        setNextDisabled(group === '');
        break;
      case 2: // disciplines (optional)
        setNextDisabled(false);
        break;
      default:
        break;
    }
  }, [activeStep, group, info]);

  const isStepOptional = step => {
    return step === 2;
  };

  const isStepSkipped = step => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep(prevActiveStep => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const renderActiveStep = () => {
    switch (activeStep) {
      case 0:
        return <StudentInfoView info={info} setInfo={setInfo} />;
      case 1:
        return <SelectGroupView group={group} setGroup={setGroup} groups={groups} />;
      // case 2:
      //   return (
      //     <SelectDisciplinesView
      //       allDisciplines={allDisciplines}
      //       otherDisciplines={otherDisciplines}
      //       isDisciplineChosen={isDisciplineChosen}
      //       onChooseDiscipline={handleChooseDiscipline}
      //       addOtherDiscipline={addOtherDiscipline}
      //     />
      //   );
      default:
        return <></>;
    }
  };

  const classes = useStyles();

  return (
    <div>
      <Stepper className={classes.stepper} activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = {};
          if (isStepOptional(index)) {
            labelProps.optional = <Typography variant="caption">Optional</Typography>;
          }
          if (isStepSkipped(index)) {
            stepProps.completed = false;
          }
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
      <Container maxWidth={false} className={classes.background}>
        {activeStep === steps.length ? (
          <FinishedView />
        ) : (
          <Card className={classes.card}>
            <Grid container direction="column" justify="center" alignItems="center" spacing={6}>
              <Grid item xs={12}>
                {renderActiveStep()}
              </Grid>
              <Grid className={classes.buttonBar} item xs={12}>
                <Button
                  disabled={activeStep === 0}
                  variant="outlined"
                  color="primary"
                  onClick={handleBack}
                >
                  Back
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  disabled={nextDisabled}
                  onClick={handleNext}
                >
                  {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                </Button>
              </Grid>
            </Grid>
          </Card>
        )}
      </Container>
    </div>
  );
};

export default FirstTimeContainer;
