import React, { useState, useEffect } from 'react';
import { FormControl, InputLabel, Select, MenuItem, makeStyles, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import { years, languages, undergradProgrammeList } from '../../../../utils/hardcoded';

const useStyles = makeStyles(() => ({
  formControl: {
    width: 200,
  },
}));

const StudentInfoView = ({ info, setInfo }) => {
  const classes = useStyles();

  const [programmeMenuItems, setProgrammeMenuItems] = useState(<MenuItem value={0}>null</MenuItem>);

  const renderYearMenuItems = () =>
    years.map(year => <MenuItem value={year.id}>{year.title}</MenuItem>);

  const renderLanguageMenuItems = () =>
    languages.map(language => <MenuItem value={language.title}>{language.title}</MenuItem>);

  useEffect(() => {
    if (info.language !== '' && info.year !== '') {
      setProgrammeMenuItems(
        undergradProgrammeList
          .filter(category => category.language === info.language)[0]
          .list.map(programme => <MenuItem value={programme.title}>{programme.title}</MenuItem>),
      );
    }
  }, [info]);
  return (
    <Grid container direction="column" justify="center" alignItems="center" spacing={3}>
      <Grid item xs={12}>
        <FormControl required variant="filled" className={classes.formControl}>
          <InputLabel id="year-select-label">Year of Study</InputLabel>
          <Select
            labelId="year-select-label"
            id="year-select"
            value={info.year}
            onChange={event => setInfo({ ...info, year: event.target.value })}
          >
            {renderYearMenuItems()}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl required variant="filled" className={classes.formControl}>
          <InputLabel id="lang-select-label">Language</InputLabel>
          <Select
            labelId="lang-select-label"
            id="lang-select"
            value={info.language}
            onChange={event => setInfo({ ...info, language: event.target.value })}
          >
            {renderLanguageMenuItems()}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl
          disabled={info.language === '' || info.year === ''}
          required
          variant="filled"
          className={classes.formControl}
        >
          <InputLabel id="programme-select-label">Programme</InputLabel>
          <Select
            labelId="programme-select-label"
            id="programme-select"
            value={info.programme}
            onChange={event => setInfo({ ...info, programme: event.target.value })}
          >
            {programmeMenuItems}
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  );
};

StudentInfoView.propTypes = {
  info: PropTypes.shape({
    year: PropTypes.isRequired,
    programme: PropTypes.isRequired,
    language: PropTypes.isRequired,
  }).isRequired,
  setInfo: PropTypes.func.isRequired,
};

export default StudentInfoView;
