import React from 'react';
import {
  FormControl,
  makeStyles,
  Radio,
  FormControlLabel,
  FormLabel,
  RadioGroup,
  Box,
  Divider,
} from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  formControl: {
    minWidth: 200,
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

const SelectGroupView = ({ group, setGroup, groups }) => {
  const classes = useStyles();

  const renderGroupRadio = () => {
    return groups.map(g => (
      <>
        {groups.indexOf(g) !== 0 && <Divider />}
        <FormControlLabel value={g.toString()} control={<Radio />} label={g} />
      </>
    ));
  };

  return (
    <Box className={classes.container}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel style={{ textAlign: 'center' }} component="legend">
          Groups
        </FormLabel>
        <RadioGroup
          aria-label="group"
          name="group"
          value={group}
          onChange={event => setGroup(event.target.value)}
        >
          {renderGroupRadio()}
        </RadioGroup>
      </FormControl>
    </Box>
  );
};

SelectGroupView.propTypes = {
  group: PropTypes.string.isRequired,
  setGroup: PropTypes.func.isRequired,
  groups: PropTypes.array.isRequired,
};

export default SelectGroupView;
