/* eslint-disable radix */
import React, { useState } from 'react';
import {
  FormControl,
  makeStyles,
  FormControlLabel,
  FormLabel,
  Box,
  Divider,
  Checkbox,
  FormGroup,
  Select,
  InputLabel,
  Input,
  IconButton,
} from '@material-ui/core';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  formControl: {
    minWidth: 200,
  },
  container: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'column',
  },
  addOthersContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    width: '100%',
  },
}));

const SelectDisciplinesView = ({
  allDisciplines,
  otherDisciplines,
  isDisciplineChosen,
  onChooseDiscipline,
  addOtherDiscipline,
}) => {
  const classes = useStyles();

  const [selectedOther, setSelectedOther] = useState({ id: '', title: '' });

  const renderDisciplinesCheckbox = () => {
    return allDisciplines.map(discipline => (
      <>
        {allDisciplines.indexOf(discipline) !== 0 && <Divider />}
        <FormControlLabel
          control={
            <Checkbox
              value={discipline.id}
              checked={isDisciplineChosen(discipline.id)}
              onChange={onChooseDiscipline(discipline.id)}
            />
          }
          label={discipline.title}
        />
      </>
    ));
  };

  const renderOtherDisciplines = () => {
    return otherDisciplines.map(({ category, list }) => (
      <optgroup label={category}>
        {list.map(({ id, title }) => {
          if (selectedOther.id === '') setSelectedOther({ id, title });
          return <option value={id}>{title}</option>;
        })}
      </optgroup>
    ));
  };

  return (
    <Box className={classes.container}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Common disciplines</FormLabel>
        <FormGroup>{renderDisciplinesCheckbox()}</FormGroup>
      </FormControl>
      <FormControl>
        <InputLabel htmlFor="other-disciplines-select">Other disciplines</InputLabel>
        <Box className={classes.addOthersContainer}>
          <Select
            native
            value={selectedOther.id}
            onChange={event =>
              setSelectedOther({
                id: parseInt(event.target.value),
                title: event.nativeEvent.target[event.nativeEvent.target.selectedIndex].text,
              })
            }
            input={<Input id="other-disciplines-select" />}
          >
            {renderOtherDisciplines()}
          </Select>
          <IconButton
            aria-label="delete"
            className={classes.margin}
            onClick={() => addOtherDiscipline(selectedOther)}
          >
            <PlaylistAddIcon color="secondary" />
          </IconButton>
        </Box>
      </FormControl>
    </Box>
  );
};

SelectDisciplinesView.propTypes = {
  allDisciplines: PropTypes.array.isRequired,
  otherDisciplines: PropTypes.array.isRequired,
  isDisciplineChosen: PropTypes.array.isRequired,
  onChooseDiscipline: PropTypes.func.isRequired,
  addOtherDiscipline: PropTypes.func.isRequired,
};
export default SelectDisciplinesView;
