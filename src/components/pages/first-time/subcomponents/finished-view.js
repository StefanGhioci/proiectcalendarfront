/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import { DoneOutline } from '@material-ui/icons';
import { Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: 'white',
    textShadow: 'black 0px 0px 5px',
  },
  link: {
    color: 'white',
    textDecoration: 'underline',
    textShadow: 'black 0px 0px 5px',
    cursor: 'pointer',
  },
  icon: { fontSize: '20vh', color: theme.palette.primary.main },
}));

const FinishedView = () => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Typography className={classes.title} variant="h4" gutterBottom>
        You're all set!
      </Typography>
      <DoneOutline className={classes.icon} />
      <Link className={classes.link} variant="h6" href="/user">
        Go back to home page
      </Link>
    </div>
  );
};

export default FinishedView;
