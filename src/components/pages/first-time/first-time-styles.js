import { makeStyles } from '@material-ui/styles';
import { grey } from '@material-ui/core/colors';

export const useStyles = makeStyles(() => ({
  buttonBar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: 240,
  },
  card: {
    margin: 'auto',
    padding: '5vh',
    height: 'auto',
  },
  background: {
    boxShadow: 'inset 1px 4px 9px -6px',
    height: '87.5vh',
    display: 'flex',
    justifyContent: 'space-evenly',
    // TODO: find background image
    backgroundColor: grey[700],
    backgroundImage: "url('/assets/images/setup-background.jpg')",
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  stepper: {
    height: '12.5vh',
  },
}));
