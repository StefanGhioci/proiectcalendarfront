import { makeStyles } from '@material-ui/styles';
import { red } from '@material-ui/core/colors';

export const useStyles = makeStyles(theme => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(5),
  },
  google: {
    backgroundColor: red[700],
    color: theme.palette.common.white,
    '&:hover': {
      backgroundColor: red[900],
    },
  },
}));
