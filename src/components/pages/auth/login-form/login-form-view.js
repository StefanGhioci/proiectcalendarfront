import React, { useEffect } from 'react';
import { loadCSS } from 'fg-loadcss';
import { Button, Grid, Icon } from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import { TextField } from 'final-form-material-ui';
import PropTypes from 'prop-types';
import { useStyles } from './login-form-styles';

const LoginView = props => {
  const { onSubmit } = props;
  const classes = useStyles();

  useEffect(() => {
    loadCSS(
      'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
      document.querySelector('#font-awesome-css'),
    );
  }, []);

  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit, submitting }) => (
        <form onSubmit={handleSubmit} className={classes.form}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Field
                component={TextField}
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                type="email"
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                variant="outlined"
                required
                fullWidth
                id="password"
                type="password"
                label="Password"
                name="password"
                autoComplete="password"
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={submitting}
              >
                Sign In
              </Button>
            </Grid>
            <Grid item xs={12}>
              <Button
                fullWidth
                className={classes.google}
                startIcon={<Icon className="fab fa-google" />}
                variant="contained"
              >
                Sign In with Google
              </Button>
            </Grid>
          </Grid>
        </form>
      )}
    />
  );
};

LoginView.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default LoginView;
