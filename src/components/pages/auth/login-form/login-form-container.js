import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import LoginView from './login-form-view';
import { AuthContext } from '../../../../context/auth/authContext';
import { apiRequestLogin } from '../../../../utils/dataprovider';
import { LOGIN_PAGE_URL } from '../../../../api/urlConstants';

const LoginForm = props => {
  const authContext = useContext(AuthContext);
  const { showMessage } = props;

  const handleSubmit = async values => {
    try {
      const userData = await apiRequestLogin(LOGIN_PAGE_URL, 'get', {
        email: values.email,
        password: values.password,
      });
      const { Bearer, student } = userData;
      authContext.setAuthState({ bearer: Bearer, username: values.email, student });
    } catch (err) {
      // ceva eroare ca username/password nu coincid/exista
      showMessage('ERROR');
      // eslint-disable-next-line no-console
      console.log(err);
    }
  };

  return <LoginView onSubmit={handleSubmit} />;
};

LoginForm.propTypes = {
  showMessage: PropTypes.func.isRequired,
};

export default LoginForm;
