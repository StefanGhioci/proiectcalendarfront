import { makeStyles } from "@material-ui/styles";
import { green, red } from "@material-ui/core/colors";

export const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh",
    margin: "auto"
  },
  image: {
    backgroundImage: "url('/assets/images/placeholder_background.jpg')",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  panel: {
    margin: theme.spacing(4, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  title: {
    textAlign: "center"
  },
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: red[700]
  },
  message: {
    display: "flex",
    alignItems: "center"
  },
  icon: {
    fontSize: 20,
    opacity: 0.9,
    marginRight: theme.spacing(1)
  }
}));
