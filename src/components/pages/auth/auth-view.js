/* eslint-disable react/jsx-wrap-multilines */
import React, { useState } from 'react';
import {
  Paper,
  Grid,
  Box,
  Tabs,
  Tab,
  Typography,
  Snackbar,
  SnackbarContent,
} from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import RegisterForm from './register-form/register-form-container';
import { useStyles } from './auth-styles';
import LoginForm from './login-form/login-form-container';

const AuthView = () => {
  const classes = useStyles();
  const [tabValue, setTabValue] = useState(1);
  const [snackbar, setSnackbar] = useState({
    open: false,
    message: '',
    error: false,
  });

  const showSnackBar = type => {
    if (type === 'SUCCESS')
      setSnackbar({
        open: true,
        message: 'Registration complete!',
        error: false,
      });
    if (type === 'ERROR')
      setSnackbar({
        open: true,
        message: 'An error occured, try again :(',
        error: true,
      });
  };

  return (
    <div>
      <Grid container component="main" className={classes.root}>
        <Grid item xs={false} sm={4} md={8} className={classes.image} />
        <Grid item xs={12} sm={8} md={4} component={Paper} elevation={6} square>
          <Tabs
            value={tabValue}
            onChange={(_event, value) => setTabValue(value)}
            variant="fullWidth"
            indicatorColor="secondary"
            textColor="secondary"
            scrollButtons="desktop"
          >
            <Tab label="REGISTER" />
            <Tab label="LOGIN" />
          </Tabs>
          {tabValue === 0 ? (
            <Box className={classes.panel}>
              <Typography component="h1" variant="h4" className={classes.title}>
                Create a new account
              </Typography>
              <RegisterForm className="registerForm" showMessage={showSnackBar} />
            </Box>
          ) : (
            <Box className={classes.panel}>
              <Typography component="h1" variant="h4" className={classes.title}>
                Enter your credentials
                <LoginForm className="loginForm" showMessage={showSnackBar} />
              </Typography>
            </Box>
          )}
        </Grid>
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={snackbar.open}
        autoHideDuration={3000}
        onClose={() => setSnackbar({ ...snackbar, open: false })}
      >
        <SnackbarContent
          className={snackbar.error ? classes.error : classes.success}
          message={
            <span className={classes.message}>
              {snackbar.error ? (
                <ErrorIcon className={classes.icon} />
              ) : (
                <CheckCircleIcon className={classes.icon} />
              )}
              {snackbar.message}
            </span>
          }
        />
      </Snackbar>
    </div>
  );
};

export default AuthView;
