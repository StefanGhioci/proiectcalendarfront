import React from 'react';
import PropTypes from 'prop-types';
import RegisterView from './register-form-view';
import { REGISTER_URL } from '../../../../api/urlConstants';
import { PASSWORD_REGEX } from '../../../../utils/constants';
import { apiRequestUnauthenticated } from '../../../../utils/dataprovider';

const RegisterForm = props => {
  const { showMessage } = props;
  const handleSubmit = async values => {
    try {
      const data = {
        email: values.email,
        password: values.password,
        firstName: values.firstName,
        lastName: values.lastName,
      };
      await apiRequestUnauthenticated(REGISTER_URL, 'post', data);
      showMessage('SUCCESS');
    } catch (error) {
      showMessage('ERROR');
    }
  };

  const handleValidation = values => {
    const errors = {};
    if (!values.password || values.password.length < 6 || !PASSWORD_REGEX.test(values.password)) {
      errors.password = 'Password must at least be 8-15 characters long.';
    }
    if (values.password !== values.password2) {
      errors.password2 = "Passwords don't match";
    }
    return errors;
  };

  return <RegisterView onSubmit={handleSubmit} validate={handleValidation} />;
};

RegisterForm.propTypes = {
  showMessage: PropTypes.func.isRequired,
};

export default RegisterForm;
