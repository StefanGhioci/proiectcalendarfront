import React, { useEffect } from 'react';
import { loadCSS } from 'fg-loadcss';
import { Button, Grid, Icon } from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import { TextField } from 'final-form-material-ui';
import PropTypes from 'prop-types';
import { useStyles } from './register-form-styles';

const RegisterView = props => {
  const { onSubmit, validate } = props;
  const classes = useStyles();

  useEffect(() => {
    loadCSS(
      'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
      document.querySelector('#font-awesome-css'),
    );
  }, []);

  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      render={({ handleSubmit, submitting }) => (
        <form onSubmit={handleSubmit} className={classes.form}>
          <Grid container spacing={2}>
            <Grid item sm={6} xs={12}>
              <Field
                component={TextField}
                autoComplete="given-name"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                autoFocus
              />
            </Grid>
            <Grid item sm={6} xs={12}>
              <Field
                component={TextField}
                autoComplete="family-name"
                name="lastName"
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                type="email"
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                variant="outlined"
                required
                fullWidth
                id="password"
                type="password"
                label="Password"
                name="password"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                component={TextField}
                variant="outlined"
                required
                fullWidth
                type="password"
                id="password2"
                label="Confirm Password"
                name="password2"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={submitting}
              >
                Sign Up
              </Button>
            </Grid>
            <Grid item xs={12}>
              <Button
                fullWidth
                className={classes.google}
                startIcon={<Icon className="fab fa-google" />}
                variant="contained"
              >
                Sign Up with Google
              </Button>
            </Grid>
          </Grid>
        </form>
      )}
    />
  );
};

RegisterView.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  validate: PropTypes.func.isRequired,
};

export default RegisterView;
