import React, {useContext, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Box} from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import FullCalendar from '@fullcalendar/react';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import timeGridPlugin from '@fullcalendar/timegrid';
import {AuthContext} from '../../../context/auth/authContext';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import EventContainer from './event/event-container';
import {useStyles} from './homepage-styles';
import {useEvents} from '../../../context/events/eventContext';
import {GET_EVENTS_URL} from "../../../api/urlConstants";
import {apiRequestPathParams} from "../../../utils/dataprovider";

const HomepageView = props => {
  const {createEvent} = props;
  // eslint-disable-next-line no-unused-vars
  const authContext = useContext(AuthContext); // will be needed for api calls i think?
  const [{events}, dispatch] = useEvents();
  const [calendarEvents, setCalendarEvents] = useState([events]);
  const [showDetailsOverlay, setShowDetailsOverlay] = useState(false);
  const [selectedEvent, setSelectedEvent] = useState();
  const classes = useStyles();

  useEffect(() => {
    // this should retrieve the data from the server somehow
    apiRequestPathParams(GET_EVENTS_URL, "GET", null, [{name: "userEmail", value: authContext.auth.username}])
      .then((data) => {
        let dataToPut = [];
        console.log(data);
        !data.error && data.forEach(event => {
          const startDate = new Date("2019-09-30");
          const endDate = new Date("2020-01-18");
          let showed = 0;
          if (event.frequency === "Sapt2") {
            startDate.setDate(startDate.getDate() + 1);
          }
          switch (event.day) {
            case "Marti":
              startDate.setDate(startDate.getDate() + 1);
              break;
            case "Miercuri":
              startDate.setDate(startDate.getDate() + 2);
              break;
            case "Joi":
              startDate.setDate(startDate.getDate() + 3);
              break;
            case "Vineri":
              startDate.setDate(startDate.getDate() + 4);
              break;
            default:
              break;
          }
          while (startDate <= endDate) {
            if (startDate.getDay() === 6 || startDate.getDay() === 0) {
              break;
            }
            const eventStartDate = new Date(startDate);
            const eventEndDate = new Date(startDate);
            eventStartDate.setHours(event.startHour);
            eventEndDate.setHours(event.startHour + event.douration);
            dataToPut = dataToPut.concat(createEvent(event.id + "_" + showed,
              event.activityType + ": " + event.disciplineName,
              eventStartDate,
              eventEndDate,
              "Formatia: " + event.formation + "\n" + event.room + "\n" + event.activityType,
              event.activityType
              )
            );
            if (event.frequency !== "") {
              startDate.setDate(startDate.getDate() + 14);
            } else {
              startDate.setDate(startDate.getDate() + 7);
            }
            showed++;
          }
        });
        setCalendarEvents(calendarEvents.concat(dataToPut));
      }).catch(() => console.log("FMM"));
  }, []);

  useEffect(() => {
    // this should also send the new event to the server
    dispatch({
      type: 'setEvents',
      events: calendarEvents,
    });
  }, [dispatch, calendarEvents]);

  const toggleOverlay = () => {
    setShowDetailsOverlay(!showDetailsOverlay);
  };

  const handleDateClick = arg => {
    setSelectedEvent(createEvent(-1, 'New Event', arg.date, arg.date, 'Description'));
    toggleOverlay();
  };

  const handleEventClick = arg => {
    setSelectedEvent(arg.event);
    console.log(arg.event);
    toggleOverlay();
  };

  const handleDelete = () => {
    console.log("SELECTED ID:", selectedEvent.id);
    setCalendarEvents(calendarEvents.filter(event => event.id !== selectedEvent.id));
    toggleOverlay();
  };

  const handleSave = (id, title, startDate, endDate, description) => {
    if (id === -1) {
      // if no event exist, create one
      setCalendarEvents(
        calendarEvents.concat(
          createEvent(new Date().toISOString(), title, startDate, endDate, description),
        ),
      );
    } else {
      setCalendarEvents(
        calendarEvents
        // if event exists modify it and save it
          .filter(event => event.id !== id)
          .concat(createEvent(id, title, startDate, endDate, description)),
      );
    }
    toggleOverlay();
  };

  return (
    <Box>
      <Modal
        className={classes.modal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={showDetailsOverlay}
        onClose={toggleOverlay}
      >
        <div className={classes.paper}>
          <EventContainer
            event={selectedEvent}
            handleDelete={handleDelete}
            handleSave={handleSave}
          />
        </div>
      </Modal>
      <Box>
        <FullCalendar
          schedulerLicenseKey="GPL-My-Project-Is-Open-Source"
          header={{
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek',
          }}
          defaultView="dayGridMonth"
          plugins={[dayGridPlugin, resourceTimelinePlugin, interactionPlugin, timeGridPlugin]}
          events={calendarEvents}
          dateClick={handleDateClick}
          eventClick={handleEventClick}
        />
      </Box>
    </Box>
  );
};
export default HomepageView;

HomepageView.propTypes = {
  createEvent: PropTypes.func.isRequired,
};
