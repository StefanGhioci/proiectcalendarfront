import React, { useState } from 'react';
import { Box } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import { useStyles } from './event-styles';

const EventView = props => {
  // eslint-disable-next-line no-unused-vars
  const classes = useStyles();
  const { event, handleDelete, handleSave } = props;
  const [title, setTitle] = useState(event.title || '');
  const [startDate, setStartDate] = useState(event.start || new Date());
  const [endDate, setEndDate] = useState(event.end || null);
  const [description, setDescription] = useState(event.extendedProps.description || '');

  const doSave = () => {
    handleSave(title, startDate, endDate, description);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Box width={1}>
        <h2>
          <TextField
            fullWidth
            label="Title"
            value={title}
            onChange={evt => setTitle(evt.target.value)}
          />
        </h2>
        <Box>
          <DateTimePicker label="Start" value={startDate} onChange={setStartDate} />
        </Box>
        <Box>
          <DateTimePicker label="End" value={endDate} onChange={setEndDate} />
        </Box>
        <Box>
          <TextField
            fullWidth
            id="outlined-multiline-static"
            multiline
            label="Description"
            rows="4"
            defaultValue={description}
            onChange={setDescription}
          />
        </Box>
        <Box m={1}>
          <Button onClick={doSave} color="primary" variant="contained" startIcon={<SaveIcon />}>
            Save
          </Button>
          <Button
            onClick={handleDelete}
            color="secondary"
            variant="contained"
            startIcon={<DeleteIcon />}
            style={{ float: 'right' }}
          >
            Delete
          </Button>
        </Box>
      </Box>
    </MuiPickersUtilsProvider>
  );
};

export default EventView;

EventView.propTypes = {
  event: PropTypes.object.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired,
};
