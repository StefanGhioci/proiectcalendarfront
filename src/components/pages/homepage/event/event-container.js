import React from 'react';
import PropTypes from 'prop-types';
import EventView from './event-view';

const EventContainer = props => {
  const handleSaveWrapper = (title, startDate, endDate, description) => {
    const { event, handleSave } = props;
    handleSave(event.id, title, startDate, endDate, description);
  };

  return <EventView {...props} handleSave={handleSaveWrapper}/>;
};

export default EventContainer;
EventContainer.propTypes = {
  event: PropTypes.object.isRequired,
  handleSave: PropTypes.func.isRequired,
};
