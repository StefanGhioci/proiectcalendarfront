import React from 'react';
import HomepageView from './homepage-view';
import EventProvider from '../../../context/events/eventContext';

const HomepageContainer = () => {
  const createEvent = (id, title, start, end, description, activityType) => {
    let color;
    switch(activityType) {
      case "Curs":
        color = "#991111";
        break;
      case "Laborator":
        color = "#161199";
        break;
      default:
        color = "#11998b";
        break;
    }
    return {
      id,
      title,
      start,
      end,
      allDay: false,
      backgroundColor: color,
      extendedProps: {
        // aici se poate pune orice
        description,
      },
    };
  };

  return (
    <EventProvider>
      <HomepageView createEvent={createEvent} />
    </EventProvider>
  );
};

export default HomepageContainer;
