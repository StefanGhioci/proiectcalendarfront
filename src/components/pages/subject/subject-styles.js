import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    margin: '-24px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    overflowX: 'hidden',
    backgroundImage: "url('/assets/images/subject-background.jpg')",
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundColor: theme.palette.background.default,
    backgroundAttachment: 'fixed',
  },
  panel: {
    width: '85%',
    backgroundColor: theme.palette.background.default,
    '&:hover': {
      background: theme.palette.background.paper,
      transition: 'all 0.2s',
    },
  },
  dialog: {
    // display: 'flex',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  closeBtn: {
    position: 'absolute',
    top: theme.spacing(2),
    right: theme.spacing(2),
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #000',
    // boxShadow: theme.shadows[3],
    padding: theme.spacing(2, 4, 3),
    position: 'relative',
  },
  expanded: {
    width: '85%',
    margin: '0 !important',
    backgroundColor: theme.palette.background.paper,
  },
  heading: {
    color: theme.palette.text.primary,
    flexBasis: '40%',
    flexShrink: 0,
  },
  secondaryHeading: {
    color: theme.palette.text.secondary,
  },
  indent: { marginTop: 5, marginLeft: 10 },
  panelDetails: {
    color: theme.palette.text.primary,
    display: 'flex',
    flexFlow: 'column',
    // justifyContent: 'space-between',
  },
  accentText: {
    color: theme.palette.secondary.main,
  },
  info: {
    minWidth: 400,
    marginBottom: 25,
    // '&:hover': {
    //   background: theme.palette.background.default,
    //   transition: 'all 0.2s',
    // },
  },
  nextActivity: {
    minWidth: 300,
    marginBottom: 25,
    // '&:hover': {
    //   background: theme.palette.background.default,
    //   transition: 'all 0.2s',
    // },
  },
  nextHomework: {
    // position: 'relative',
    minWidth: 400,
    marginBottom: 25,
    // '&:hover': {
    //   background: theme.palette.background.default,
    //   transition: 'all 0.2s',
    // },
  },
  nextHomeworkAddBtn: {
    marginTop: theme.spacing(2),
    // position: 'absolute',
    // bottom: theme.spacing(2),
    // right: theme.spacing(2),
  },
  nextExam: {
    minWidth: 275,
    // '&:hover': {
    //   background: theme.palette.background.default,
    //   transition: 'all 0.2s',
    // },
  },
  nextExamAddBtn: {
    marginTop: theme.spacing(2),
    // position: 'absolute',
    // bottom: theme.spacing(2),
    // right: theme.spacing(2),
  },
}));
