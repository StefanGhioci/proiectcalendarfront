import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Divider, Typography, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { useSubject } from '../../../../context/subject';
import AddHomeworkDialog from './AddHomeworkDialog';

const NextHomework = ({ index }) => {
  const [{ subjects, classes }] = useSubject();
  const [popupOpen, setPopupOpen] = useState(false);

  const openPopup = () => setPopupOpen(true);
  const closePopup = () => setPopupOpen(false);

  return (
    <>
      <AddHomeworkDialog index={index} open={popupOpen} closePopup={closePopup} />
      <div className={classes.nextHomework}>
        <Typography variant="h5">Next Homework:</Typography>
        <div className={classes.indent}>
          {subjects[index].nextHomework.title && (
            <Typography>
              <b className={classes.accentText}>Title:</b>
              {` ${subjects[index].nextHomework.title}`}
            </Typography>
          )}
          {subjects[index].nextHomework.text && (
            <Typography>
              <b className={classes.accentText}>Text:</b>
              {` ${subjects[index].nextHomework.text}`}
            </Typography>
          )}
          {subjects[index].nextHomework.deadline && (
            <Typography>
              <b className={classes.accentText}>Deadline:</b>
              {` ${subjects[index].nextHomework.deadline}`}
            </Typography>
          )}
        </div>
        <Button
          color="secondary"
          className={classes.nextHomeworkAddBtn}
          startIcon={<AddIcon />}
          onClick={openPopup}
        >
          Add Homework
        </Button>
        <Divider variant="middle" light style={{ height: 2, marginTop: 10 }} />
      </div>
    </>
  );
};

NextHomework.propTypes = {
  index: PropTypes.number.isRequired,
};

export default NextHomework;
