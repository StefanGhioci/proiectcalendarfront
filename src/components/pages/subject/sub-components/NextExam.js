import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Typography, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { useSubject } from '../../../../context/subject';
import AddExamDialog from './AddExamDialog';

const NextExam = ({ index }) => {
  const [{ subjects, classes }] = useSubject();
  const [popupOpen, setPopupOpen] = useState(false);

  const openPopup = () => setPopupOpen(true);
  const closePopup = () => setPopupOpen(false);

  return (
    <>
      <AddExamDialog open={popupOpen} closePopup={closePopup} index={index} />
      <div className={classes.nextExam}>
        <Typography variant="h5">Next Exam:</Typography>
        <div className={classes.indent}>
          {subjects[index].nextExam.type && (
            <Typography>
              <b className={classes.accentText}>Type:</b>
              {` ${subjects[index].nextExam.type}`}
            </Typography>
          )}
          {subjects[index].nextExam.date && (
            <Typography>
              <b className={classes.accentText}>Date:</b>
              {` ${subjects[index].nextExam.date}`}
            </Typography>
          )}
          {subjects[index].nextExam.room && (
            <Typography>
              <b className={classes.accentText}>Room:</b>
              {` ${subjects[index].nextExam.room}`}
            </Typography>
          )}
          {subjects[index].nextExam.notes && (
            <Typography>
              <b className={classes.accentText}>Notes:</b>
              {` ${subjects[index].nextExam.notes}`}
            </Typography>
          )}
          <Button
            // variant="contained"
            color="secondary"
            className={classes.nextExamAddBtn}
            startIcon={<AddIcon />}
            onClick={openPopup}
          >
            Add Exam
          </Button>
        </div>
      </div>
    </>
  );
};

NextExam.propTypes = {
  index: PropTypes.number.isRequired,
};

export default NextExam;
