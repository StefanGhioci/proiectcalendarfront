import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Divider, Link } from '@material-ui/core';
import { useSubject } from '../../../../context/subject';

const Info = ({ index }) => {
  const [{ subjects, classes }] = useSubject();

  return (
    <div className={classes.info}>
      <div className={classes.indent}>
        <Typography>
          <b className={classes.accentText}>Teacher:</b>
          {` ${subjects[index].teacher.name}`}
        </Typography>
        <Typography>
          <b className={classes.accentText}>Email:</b>
          {` ${subjects[index].teacher.email}`}
        </Typography>
        <Typography>
          <b className={classes.accentText}>Website: </b>
          <Link
            href={subjects[index].website.href}
            color="inherit"
            target="_blank"
            rel="noreferrer"
          >
            {subjects[index].website.pathname}
          </Link>
        </Typography>
      </div>
      <Divider variant="middle" light style={{ height: 2, marginTop: 10 }} />
    </div>
  );
};
Info.propTypes = {
  index: PropTypes.number.isRequired,
};
export default Info;
