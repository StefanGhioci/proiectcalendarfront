import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  MenuItem,
} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import { Form, Field } from 'react-final-form';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDateTimePicker } from '@material-ui/pickers';
import { TextField } from 'final-form-material-ui';
import { useSubject } from '../../../../context/subject';
import { apiRequest } from '../../../../utils/dataprovider';
import { AuthContext } from '../../../../context/auth/authContext';

export const rooms = [
  '2/I',
  'L306',
  'L336',
  'C335',
  'L338',
  'L301',
  'L402',
  'C510',
  'L002',
  'C310',
  'L534',
  'A303',
  'L001',
  'L439',
  'L302',
  'A322',
  'C514',
  'A311',
  'A308',
  'Goanga',
  'A313',
  'bogrea',
  'AAM',
  '7/I',
  '5/I',
  'A321',
  'A2',
  '6/II',
  'L339',
  'C512',
  'L308',
  'A312',
  'L321',
  'L343',
  'L320',
  '9/I',
  'Catedra',
  'e',
  'gamma',
  'Multimedia',
  'L307',
  'pi',
  'MOS-S15',
  'theta',
  'Obs',
  'Platon',
  'bratianu',
  'AVM',
  'lambda',
  'phi',
  'kilimanjar',
  'NTTSocrate',
  'sala-MHP',
  'A323',
  'KuTi',
];

const AddExamDialog = ({ open, closePopup, index }) => {
  const [{ subjects, classes }, dispatch] = useSubject();
  const { auth } = useContext(AuthContext);

  const [selectedDate, setSelectedDate] = React.useState(new Date());

  const handleDateChange = date => setSelectedDate(date);
  const saveExam = async values => {
    apiRequest('/exam', 'POST', {
      studentEmail: auth.username,
      disciplineId: parseInt(subjects[index].id),
      type: values.type,
      date: selectedDate.toLocaleString(),
      room: values.room,
      notes: values.notes,
    });
    await dispatch({
      type: 'setNextExam',
      id: subjects[index].id,
      nextExam: {
        type: values.type,
        date: selectedDate.toLocaleString(),
        room: values.room,
        notes: values.notes,
      },
    });
    closePopup();
  };
  const validateExam = values => {
    const errors = {};
    if (!values.type) {
      errors.type = 'You need to set a type for the exam';
    }
    if (!values.room) {
      errors.room = 'You need to set a room for the exam';
    }
    return errors;
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Dialog
        className={classes.dialog}
        open={open}
        onClose={closePopup}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Add exam</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To add a exam to this subject, please select the type, date and room of it. It will be
            displayed in the next exam section.
          </DialogContentText>
          <Form
            onSubmit={saveExam}
            validate={validateExam}
            render={({ handleSubmit, submitting }) => (
              <form onSubmit={handleSubmit} className={classes.form}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Field
                      component={TextField}
                      variant="outlined"
                      fullWidth
                      select
                      id="type"
                      label="Type"
                      name="type"
                      autoComplete="type"
                      autoFocus
                    >
                      {['practic', 'colocviu', 'partial', 'scris'].map(option => (
                        <MenuItem key={option} value={option}>
                          {option}
                        </MenuItem>
                      ))}
                    </Field>
                  </Grid>
                  <Grid item xs={12}>
                    <KeyboardDateTimePicker
                      id="date-picker"
                      label="Date"
                      format="dd/MM/yyyy - hh:mm"
                      inputVariant="outlined"
                      fullWidth
                      value={selectedDate}
                      onChange={handleDateChange}
                      showTodayButton
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      component={TextField}
                      variant="outlined"
                      fullWidth
                      select
                      id="room"
                      label="Room"
                      name="room"
                      autoComplete="room"
                    >
                      {rooms.map(option => (
                        <MenuItem key={option} value={option}>
                          {option}
                        </MenuItem>
                      ))}
                    </Field>
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      component={TextField}
                      variant="outlined"
                      fullWidth
                      id="notes"
                      label="Notes"
                      name="notes"
                      autoComplete="notes"
                      rows={4}
                      multiline
                      placeholder="Enter some notes about the exam here"
                    />
                  </Grid>
                </Grid>
                <DialogActions>
                  <Button
                    type="submit"
                    color="secondary"
                    className={classes.nextExamAddBtn}
                    startIcon={<SaveIcon />}
                    disabled={submitting}
                  >
                    Save
                  </Button>
                  <Button
                    color="secondary"
                    className={classes.nextExamAddBtn}
                    startIcon={<CloseIcon />}
                    disabled={submitting}
                    onClick={closePopup}
                  >
                    Cancel
                  </Button>
                </DialogActions>
              </form>
            )}
          />
        </DialogContent>
      </Dialog>
    </MuiPickersUtilsProvider>
  );
};

AddExamDialog.propTypes = {
  index: PropTypes.number.isRequired,
  open: PropTypes.bool.isRequired,
  closePopup: PropTypes.func.isRequired,
};

export default AddExamDialog;
