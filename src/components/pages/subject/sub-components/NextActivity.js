import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Divider } from '@material-ui/core';
import { useSubject } from '../../../../context/subject';

const NextActivity = ({ index }) => {
  const [{ subjects, classes }] = useSubject();

  return (
    <div className={classes.nextActivity}>
      <Typography variant="h5">Next Activity:</Typography>
      <div className={classes.indent}>
        <Typography>
          <b className={classes.accentText}>Type:</b>
          {` ${subjects[index].nextActivity.type}`}
        </Typography>
        <Typography>
          <b className={classes.accentText}>Date:</b>
          {` ${subjects[index].nextActivity.date}`}
        </Typography>
        <Typography>
          <b className={classes.accentText}>Room:</b>
          {` ${subjects[index].nextActivity.room}`}
        </Typography>
        <Typography>
          <b className={classes.accentText}>No. of hours a week:</b>
          {` ${subjects[index].nextActivity.hoursWeek}`}
        </Typography>
        <Typography>
          <b className={classes.accentText}>Absences:</b>
          {` ${subjects[index].nextActivity.absences}`}
        </Typography>
      </div>
      <Divider variant="middle" light style={{ height: 2, marginTop: 10 }} />
    </div>
  );
};

NextActivity.propTypes = {
  index: PropTypes.number.isRequired,
};

export default NextActivity;
