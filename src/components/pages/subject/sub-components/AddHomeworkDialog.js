import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import { Form, Field } from 'react-final-form';
import { TextField } from 'final-form-material-ui';
import { useSubject } from '../../../../context/subject';
import { apiRequest } from '../../../../utils/dataprovider';
import { AuthContext } from '../../../../context/auth/authContext';

const AddHomeworkDialog = ({ open, closePopup, index }) => {
  const [{ subjects, classes }, dispatch] = useSubject();
  const { auth } = useContext(AuthContext);

  const saveHomework = async values => {
    apiRequest('/homework', 'POST', {
      studentEmail: auth.username,
      disciplineId: parseInt(subjects[index].id),
      title: values.title,
      text: values.text,
      deadline: values.deadline,
    });
    await dispatch({
      type: 'setNextHomework',
      id: subjects[index].id,
      nextHomework: {
        title: values.title,
        text: values.text,
        deadline: values.deadline,
      },
    });
    closePopup();
  };
  const validateHomework = values => {
    const errors = {};
    if (!values.title) {
      errors.title = 'You need to set a title for the homework';
    }
    if (!values.text) {
      errors.text = 'You need to set a text for the homework';
    }
    if (!values.deadline) {
      errors.deadline = 'You need to set a deadline for the homework';
    }
    return errors;
  };
  return (
    <Dialog
      className={classes.dialog}
      open={open}
      onClose={closePopup}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Add homework</DialogTitle>
      <DialogContent>
        <DialogContentText>
          To add a homework to this subject, please enter the title and text of it. It will be
          displayed in the next homework section.
        </DialogContentText>
        <Form
          onSubmit={saveHomework}
          validate={validateHomework}
          render={({ handleSubmit, submitting }) => (
            <form onSubmit={handleSubmit} className={classes.form}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    variant="outlined"
                    fullWidth
                    id="title"
                    label="Title"
                    name="title"
                    autoComplete="title"
                    autoFocus
                    placeholder="Enter the homework title here"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    variant="outlined"
                    fullWidth
                    id="text"
                    label="Text"
                    name="text"
                    autoComplete="text"
                    rows={4}
                    multiline
                    placeholder="Enter the homework text here"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    component={TextField}
                    variant="outlined"
                    fullWidth
                    id="deadline"
                    label="Deadline"
                    name="deadline"
                    autoComplete="deadline"
                    autoFocus
                    placeholder="Enter the homework deadline here"
                  />
                </Grid>
              </Grid>
              <DialogActions>
                <Button
                  type="submit"
                  color="secondary"
                  className={classes.nextHomeworkAddBtn}
                  startIcon={<SaveIcon />}
                  disabled={submitting}
                >
                  Save
                </Button>
                <Button
                  color="secondary"
                  className={classes.nextHomeworkAddBtn}
                  startIcon={<CloseIcon />}
                  disabled={submitting}
                  onClick={closePopup}
                >
                  Cancel
                </Button>
              </DialogActions>
            </form>
          )}
        />
      </DialogContent>
    </Dialog>
  );
};

AddHomeworkDialog.propTypes = {
  index: PropTypes.number.isRequired,
  open: PropTypes.bool.isRequired,
  closePopup: PropTypes.func.isRequired,
};

export default AddHomeworkDialog;
