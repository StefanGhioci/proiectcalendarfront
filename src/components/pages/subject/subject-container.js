import React from 'react';
import { SnackbarProvider } from 'notistack';
import { Button } from '@material-ui/core';
import View from './subject-view';
import { SubjectProvider } from '../../../context/subject';

const SubjectContainer = () => {
  const notistackRef = React.createRef();
  const onClickDismiss = key => () => {
    notistackRef.current.closeSnackbar(key);
  };
  return (
    <SnackbarProvider
      maxSnack={10}
      ref={notistackRef}
      action={key => <Button onClick={onClickDismiss(key)}>Dismiss</Button>}
    >
      <SubjectProvider>
        <View />
      </SubjectProvider>
    </SnackbarProvider>
  );
};

export default SubjectContainer;
