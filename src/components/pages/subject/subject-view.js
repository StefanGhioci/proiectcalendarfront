import React, { useEffect, useState, useContext } from 'react';
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useSnackbar } from 'notistack';
import { useStyles } from './subject-styles';
import { useSubject } from '../../../context/subject';
import Info from './sub-components/Info';
import NextActivity from './sub-components/NextActivity';
import NextHomework from './sub-components/NextHomework';
import NextExam from './sub-components/NextExam';
import { apiRequest, apiRequestPathParams } from '../../../utils/dataprovider';
import { AuthContext } from '../../../context/auth/authContext';

export const testSubjects = [
  // HERE is a proposal for subject structure
  {
    id: 'PPD1',
    title: 'Programare Paralela si Distribuita',
    abbrevation: 'PPD',
    type: 'obligatorie',
    teacher: { name: 'Dr. Virginia Niculescu', email: 'vniculescu@cs.ubbcluj.ro' },
    website: new URL('https://www.cs.ubbcluj.ro/~vniculescu/didactic/PPD/CursPPD.html'),
    nextActivity: {
      type: 'course',
      date: 'Mon 9 Dec 2019 14:00 PM',
      room: '6/II',
      hoursWeek: 2,
      absences: 10,
    },
    nextHomework: {
      title: 'lab2 - MPI',
      text:
        // eslint-disable-next-line max-len
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      deadline: 'saptamana 3',
    },
    nextExam: {
      type: 'practic',
      date: 'Wed 11 Dec 2019 16:00 PM',
      room: 'L302',
      notes: 'tot ce s-a facut mai putin MPI',
    },
  },
  {
    id: 'PPD2',
    title: 'Programare Paralela si Distribuita 2',
    abbrevation: 'PPD',
    type: 'obligatorie',
    teacher: { name: 'Dr. Virginia Niculescu', email: 'vniculescu@cs.ubbcluj.ro' },
    website: new URL('https://www.cs.ubbcluj.ro/~vniculescu/didactic/PPD/CursPPD.html'),
    nextActivity: {
      type: 'course',
      date: 'Mon 9 Dec 2019 14:00 PM',
      room: '6/II',
      hoursWeek: 2,
      absences: 10,
    },
    nextHomework: {
      title: 'lab2 - MPI',
      text:
        // eslint-disable-next-line max-len
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      deadline: 'saptamana 3',
    },
    nextExam: {
      type: 'practic',
      date: 'Wed 11 Dec 2019 16:00 PM',
      room: 'L302',
      notes: 'tot ce s-a facut mai putin MPI',
    },
  },
  {
    id: 'PPD3',
    title: 'Programare Paralela si Distribuita 3',
    abbrevation: 'PPD',
    type: 'obligatorie',
    teacher: { name: 'Dr. Virginia Niculescu', email: 'vniculescu@cs.ubbcluj.ro' },
    website: new URL('https://www.cs.ubbcluj.ro/~vniculescu/didactic/PPD/CursPPD.html'),
    nextActivity: {
      type: 'course',
      date: 'Mon 9 Dec 2019 14:00 PM',
      room: '6/II',
      hoursWeek: 2,
      absences: 10,
    },
    nextHomework: {
      title: 'lab2 - MPI',
      text:
        // eslint-disable-next-line max-len
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      deadline: 'saptamana 3',
    },
    nextExam: {
      type: 'practic',
      date: 'Wed 11 Dec 2019 16:00 PM',
      room: 'L302',
      notes: 'tot ce s-a facut mai putin MPI',
    },
  },
  {
    id: 'PPD4',
    title: 'Programare Paralela si Distribuita 4',
    abbrevation: 'PPD',
    type: 'obligatorie',
    teacher: { name: 'Dr. Virginia Niculescu', email: 'vniculescu@cs.ubbcluj.ro' },
    website: new URL('https://www.cs.ubbcluj.ro/~vniculescu/didactic/PPD/CursPPD.html'),
    nextActivity: {
      type: 'course',
      date: 'Mon 9 Dec 2019 14:00 PM',
      room: '6/II',
      hoursWeek: 2,
      absences: 10,
    },
    nextHomework: {
      title: 'lab2 - MPI',
      text:
        // eslint-disable-next-line max-len
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      deadline: 'saptamana 3',
    },
    nextExam: {
      type: 'practic',
      date: 'Wed 11 Dec 2019 16:00 PM',
      room: 'L302',
      notes: 'tot ce s-a facut mai putin MPI',
    },
  },
  {
    id: 'PPD5',
    title: 'Programare Paralela si Distribuita 5',
    abbrevation: 'PPD',
    type: 'obligatorie',
    teacher: { name: 'Dr. Virginia Niculescu', email: 'vniculescu@cs.ubbcluj.ro' },
    website: new URL('https://www.cs.ubbcluj.ro/~vniculescu/didactic/PPD/CursPPD.html'),
    nextActivity: {
      type: 'course',
      date: 'Mon 9 Dec 2019 14:00 PM',
      room: '6/II',
      hoursWeek: 2,
      absences: 10,
    },
    nextHomework: {
      title: 'lab2 - MPI',
      text:
        // eslint-disable-next-line max-len
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      deadline: 'saptamana 3',
    },
    nextExam: {
      type: 'practic',
      date: 'Wed 11 Dec 2019 16:00 PM',
      room: 'L302',
      notes: 'tot ce s-a facut mai putin MPI',
    },
  },
  {
    id: 'PPD6',
    title: 'Programare Paralela si Distribuita 6',
    abbrevation: 'PPD',
    type: 'obligatorie',
    teacher: { name: 'Dr. Virginia Niculescu', email: 'vniculescu@cs.ubbcluj.ro' },
    website: new URL('https://www.cs.ubbcluj.ro/~vniculescu/didactic/PPD/CursPPD.html'),
    nextActivity: {
      type: 'course',
      date: 'Mon 9 Dec 2019 14:00 PM',
      room: '6/II',
      hoursWeek: 2,
      absences: 10,
    },
    nextHomework: {
      title: 'lab2 - MPI',
      text:
        // eslint-disable-next-line max-len
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      deadline: 'saptamana 3',
    },
    nextExam: {
      type: 'practic',
      date: 'Wed 11 Dec 2019 16:00 PM',
      room: 'L302',
      notes: 'tot ce s-a facut mai putin MPI',
    },
  },
];

// This is the second tab of the app.
// It will contain an accordion type list of all the subject chosen,
// when extended a subject will offer data about:
// -the next seminar/laboratory/course
// -the next homework
// -the next known exam
// -links for the teacher's website
// -the teacher's email
// -(opt) number of hours a week
// -number of available absences

const View = () => {
  const [{ subjects }, dispatch] = useSubject();
  const [expanded, setExpanded] = useState([]);
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const { auth } = useContext(AuthContext);
  // simulate a notification from server just for testing purposes
  useEffect(() => {
    setTimeout(() => {
      apiRequest('/notification/_all', 'GET', '').then(notifs =>
        notifs.forEach(({ message }) => enqueueSnackbar(message, { variant: 'info' })),
      );
    }, 2000);
  }, [enqueueSnackbar]);

  useEffect(() => {
    apiRequestPathParams('/discipline/_byWeekForStudent', 'GET', null, [
      { name: 'studentEmail', value: auth.username },
    ]).then(subjectsData => {
      dispatch({
        type: 'setSubjects',
        subjects: subjectsData,
      });
    });

    dispatch({
      type: 'setSubjectsClasses',
      classes,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, classes]);

  useEffect(() => {
    setExpanded(subjects.map((_subject, i) => i === 0));
  }, [subjects]);

  const handleChange = (i, isExpanded) =>
    setExpanded(expanded.map((exp, j) => (i === j ? isExpanded : exp)));
  return (
    <div className={classes.root}>
      {subjects.map((subject, i) => (
        <ExpansionPanel
          square
          className={expanded[i] === true ? classes.expanded : classes.panel}
          key={subject.id}
          expanded={expanded[i] === true}
          onChange={(_event, isExpanded) => handleChange(i, isExpanded)}
          TransitionProps={{ unmountOnExit: true }}
        >
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls={`${subject.id}-content`}
            id={`${subject.id}-header`}
          >
            <Typography className={classes.heading}>{subject.title}</Typography>
            {/* <Typography className={classes.secondaryHeading}>
              {'Next: '}
              {subject.nextActivity.type}
              {' on '}
              {subject.nextActivity.date}
              {' at '}
              {subject.nextActivity.room}
            </Typography> */}
          </ExpansionPanelSummary>

          <ExpansionPanelDetails className={classes.panelDetails}>
            {/* <Info index={i} /> */}
            {/* <NextActivity index={i} /> */}
            <NextHomework index={i} />
            <NextExam index={i} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
      ))}
    </div>
  );
};

export default View;
