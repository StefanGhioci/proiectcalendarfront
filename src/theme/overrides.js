//   override Material components only if strictly necessary
export default {
  MuiDivider: {
    root: {
      borderTop: 'thin solid rgba(0, 0, 0, 0.12)',
      backgroundColor: undefined,
      height: undefined,
    },
  },
};
