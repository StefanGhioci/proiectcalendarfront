const black = '#000';
const white = '#fff';

export default {
  common: { black, white },
  primary: {
    //   contrastText: ...,
    //   dark: ...,
    main: '#0015cb',
    //   light: ...
  },
  secondary: {
    main: '#fe6a07',
  },
  // success: {},
  // info: {},
  // warning: {},
  // error: {},
  // text: {},
  // background: {}
};
