import React, { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

const initialState = {
  events: [],
};

export const EventContext = createContext();

const reducer = (state, action) => {
  if (action.type === 'setEvents') {
    return {
      ...state,
      events: action.events,
    };
  }
  return state;
};

const { Provider } = EventContext;

const EventProvider = ({ children }) => {
  return <Provider value={useReducer(reducer, initialState)}>{children}</Provider>;
};

EventProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default EventProvider;
export const useEvents = () => useContext(EventContext);
