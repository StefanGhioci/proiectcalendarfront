import React, { createContext } from 'react';
import PropTypes from 'prop-types';
import { INITIAL_AUTH_STATE } from '../../utils/constants';
import useAuthHandler from './authHandler';
import { getStoredUserAuth } from '../../utils/helpers';

export const AuthContext = createContext({
  auth: INITIAL_AUTH_STATE,
  setAuthState: () => {},
  logout: () => {},
});

const { Provider } = AuthContext;

const AuthProvider = ({ children }) => {
  const { auth, setAuthState, logout } = useAuthHandler(getStoredUserAuth());
  return <Provider value={{ auth, setAuthState, logout }}>{children}</Provider>;
};

AuthProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default AuthProvider;
