import { useState } from 'react';
import { INITIAL_AUTH_STATE, LOCAL_STORAGE_AUTH } from '../../utils/constants';

const useAuthHandler = initialState => {
  const [auth, setAuth] = useState(initialState);

  const setAuthState = userAuth => {
    window.localStorage.setItem(LOCAL_STORAGE_AUTH, JSON.stringify(userAuth));
    setAuth(userAuth);
  };

  const logout = () => {
    window.localStorage.clear();
    setAuth(INITIAL_AUTH_STATE);
  };

  return {
    auth,
    setAuthState,
    logout,
  };
};
export default useAuthHandler;
