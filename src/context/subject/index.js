import React, { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

const initialState = {
  subjects: [],
  classes: {},
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'setSubjects':
      return {
        ...state,
        subjects: action.subjects,
      };
    case 'setSubjectsClasses':
      return {
        ...state,
        classes: action.classes,
      };

    case 'setNextHomework':
      return {
        ...state,
        subjects: state.subjects.map(subject => {
          if (subject.id === action.id) return { ...subject, nextHomework: action.nextHomework };
          return subject;
        }),
      };

    case 'setNextExam':
      return {
        ...state,
        subjects: state.subjects.map(subject => {
          if (subject.id === action.id) return { ...subject, nextExam: action.nextExam };
          return subject;
        }),
      };

    default:
      return state;
  }
};

export const SubjectContext = createContext();
export const SubjectProvider = ({ children }) => (
  <SubjectContext.Provider value={useReducer(reducer, initialState)}>
    {children}
  </SubjectContext.Provider>
);

SubjectProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export const useSubject = () => useContext(SubjectContext);
