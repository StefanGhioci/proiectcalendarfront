export const languages = [
  { id: 'ro', title: 'Romana' },
  { id: 'en', title: 'Engleza' },
  { id: 'hu', title: 'Maghiara' },
  { id: 'de', title: 'Germana' },
];

export const years = [
  { id: 1, title: 'Anul 1' },
  { id: 2, title: 'Anul 2' },
  { id: 3, title: 'Anul 3' },
];

export const undergradProgrammeList = [
  {
    language: 'Romana',
    list: [
      { id: 1, title: 'Informatica' },
      { id: 2, title: 'Matematica-informatica' },
      { id: 3, title: 'Matematica' },
    ],
  },
  {
    language: 'Engleza',
    list: [
      { id: 4, title: 'Informatica' },
      { id: 5, title: 'Matematica-informatica' },
    ],
  },
  {
    language: 'Maghiara',
    list: [
      { id: 6, title: 'Informatica' },
      { id: 7, title: 'Matematica-informatica' },
      { id: 8, title: 'Matematica' },
    ],
  },
  {
    language: 'Germana',
    list: [
      { id: 9, title: 'Informatica' },
      { id: 10, title: 'Matematica-informatica' },
    ],
  },
];
