/** Return user auth from local storage value */
import { INITIAL_AUTH_STATE, LOCAL_STORAGE_AUTH } from './constants';

export const getStoredUserAuth = () => {
  const auth = window.localStorage.getItem(LOCAL_STORAGE_AUTH);
  if (auth) {
    return JSON.parse(auth);
  }
  return INITIAL_AUTH_STATE;
};
