import { getStoredUserAuth } from './helpers';
import { BASE_URL } from '../api/urlConstants';

export const apiRequestLogin = async (url, method, params) => {
  const loginUrl =
    BASE_URL +
    url +
    '?email=' +
    encodeURIComponent(params.email) +
    '&password=' +
    encodeURIComponent(params.password);

  const response = await fetch(loginUrl, {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    params: {
      email: params.email,
      password: params.password,
    },
  });

  await response;

  if (response.status === 403) throw new Error('Wrong username or password');
  else if (response.status !== 200) {
    throw new Error('Internal server error ' + (await response.json()));
  }
  return response.json();
};

export const apiRequestUnauthenticated = async (url, method, bodyParams) => {
  const completeUrl = BASE_URL + url;
  const response = await fetch(completeUrl, {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: bodyParams ? JSON.stringify(bodyParams) : undefined,
  });

  // verificare daca e 401 atunci inseamna ca a expirat token-ul
  // ori trebuie refacut
  // ori trebuie redirectat la login si sters din local storage

  return response.json();
};

export const apiRequest = async (url, method, bodyParams) => {
  const useAuth = getStoredUserAuth();
  const completeUrl = BASE_URL + url;
  const response = await fetch(completeUrl, {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${useAuth.bearer}`,
    },
    body: bodyParams ? JSON.stringify(bodyParams) : undefined,
  });

  // verificare daca e 401 atunci inseamna ca a expirat token-ul
  // ori trebuie refacut
  // ori trebuie redirectat la login si sters din local storage

  return response.json();
};

export const apiRequestPathParams = async (url, method, bodyParams, pathParams) => {
  const useAuth = getStoredUserAuth();
  let completeUrl = BASE_URL + url;
  completeUrl += '?';
  pathParams.forEach((param, index) => {
    if (index === pathParams.length - 1) {
      completeUrl = completeUrl + param.name + '=' + param.value;
    } else {
      completeUrl = completeUrl + param.name + '=' + param.value + '&';
    }
  });
  const response = await fetch(completeUrl, {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${useAuth.bearer}`,
    },
    body: bodyParams ? JSON.stringify(bodyParams) : undefined,
  });

  // verificare daca e 401 atunci inseamna ca a expirat token-ul
  // ori trebuie refacut
  // ori trebuie redirectat la login si sters din local storage
  try {
    return response.json();
  } catch (ex) {
    console.log('error', ex);
    return response;
  }
};

// TODO: GET [...]
// eslint-disable-next-line no-unused-vars
export const apiRequestGroups = ({ year, programme, language }) =>
  apiRequestPathParams('/group/_all', 'GET', null, [
    { name: 'year', value: year },
    { name: 'specialization', value: programme },
    { name: 'language', value: language },
  ]);

// TODO: GET [{id: ..., title: ...}, ...]
// eslint-disable-next-line no-unused-vars
export const apiRequestAllDisciplines = async group => [];

// TODO: GET [{category: '...', list: [{id: ..., title: ...}, ...]},...]
// eslint-disable-next-line no-unused-vars
export const apiRequestOtherDisciplines = async group => [];
