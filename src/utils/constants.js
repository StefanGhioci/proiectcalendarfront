export const INITIAL_AUTH_STATE = { bearer: null, username: null, student: null };
export const LOCAL_STORAGE_AUTH = 'userAuth';
export const PASSWORD_REGEX = /^([a-zA-Z0-9@*#]{6,15})$/;

export const SECTION_CALENDAR_INDEX = 0;
export const SECTION_SUBJECTS_INDEX = 1;
