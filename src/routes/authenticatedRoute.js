import {Redirect, Route} from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';

const AuthenticatedRoute = ({component: Component, appProps, ...rest}) => {
  if (rest.path === "/first-time") {
    return <Route
      {...rest}
      render={props => {
        return appProps.isAuthenticated ? <Component {...props} {...appProps} /> : <Redirect to="/"/>
      }}
    />
  } else if (appProps.isAuthenticated && appProps.didFirstTime === false) {
    return <Route
      {...rest}
      render={props => {
        return <Redirect to="/first-time"/>
      }}
    />
  } else {
    return <Route
      {...rest}
      render={props => {
        return appProps.isAuthenticated ? <Component {...props} {...appProps} /> : <Redirect to="/"/>
      }}
    />
  }

};

AuthenticatedRoute.propTypes = {
  component: PropTypes.node.isRequired,
  appProps: PropTypes.object.isRequired,
};

export default AuthenticatedRoute;
