import React, {useContext} from 'react';
import {BrowserRouter as Router, Switch} from 'react-router-dom';
import AuthView from '../components/pages/auth/auth-view';
import UnAuthenticatedRoute from './unAuthenticatedRoute';
import {AuthContext} from '../context/auth/authContext';
import AuthenticatedRoute from './authenticatedRoute';
import FirstTimeContainer from '../components/pages/first-time/first-time-container';
import MenuContainer from '../components/menu/menu-container';
import {SECTION_CALENDAR_INDEX, SECTION_SUBJECTS_INDEX} from '../utils/constants';
import NotifContainer from '../components/notif-emit/notif-container';

const Routes = () => {
  const authContext = useContext(AuthContext);
  const isAuthenticated = authContext.auth.bearer !== null;
  const didFirstTime = authContext.auth.student === "true";
  return (
    <Router>
      <Switch>
        <UnAuthenticatedRoute exact path="/" component={AuthView} appProps={{isAuthenticated}}/>
        <UnAuthenticatedRoute
          exact
          path="/admin"
          component={NotifContainer}
          appProps={{isAuthenticated}}
        />
        <AuthenticatedRoute
          exact
          path="/user"
          component={MenuContainer}
          appProps={{isAuthenticated, didFirstTime, defaultSelectedSection: SECTION_CALENDAR_INDEX}}
        />
        <AuthenticatedRoute
          exact
          path="/first-time"
          component={FirstTimeContainer}
          appProps={{isAuthenticated, didFirstTime}}
        />
        <AuthenticatedRoute
          exact
          path="/subject"
          component={MenuContainer}
          appProps={{isAuthenticated, didFirstTime, defaultSelectedSection: SECTION_SUBJECTS_INDEX}}
        />
      </Switch>
    </Router>
  );
};

export default Routes;
