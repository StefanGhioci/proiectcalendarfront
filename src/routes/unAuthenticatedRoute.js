import { Redirect, Route } from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';

const UnAuthenticatedRoute = ({ component: Component, appProps, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      !appProps.isAuthenticated ? <Component {...props} {...appProps} /> : <Redirect to="/user" />
    }
  />
);

UnAuthenticatedRoute.propTypes = {
  component: PropTypes.node.isRequired,
  appProps: PropTypes.object.isRequired,
};

export default UnAuthenticatedRoute;
