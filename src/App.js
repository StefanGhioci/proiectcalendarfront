import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import theme from './theme';
import Routes from './routes/routes';
import AuthProvider from './context/auth/authContext';

const App = () => {
  return (
    <>
      <CssBaseline>
        <ThemeProvider theme={theme}>
          <AuthProvider>
            <Routes />
          </AuthProvider>
        </ThemeProvider>
      </CssBaseline>
    </>
  );
};
export default App;
